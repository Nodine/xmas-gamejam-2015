﻿using UnityEngine;
using System.Collections;

public class DeathZone : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        print("Im hit: " + other.gameObject.name[other.transform.name.Length - 2].ToString());
        if (other.gameObject.name[other.transform.name.Length - 2].ToString() == "0")
        {
            GameManager.Instance.DisablePlayer(other.GetComponent<PlayerInput>().getPlayerNr);
            EventManager.Instance.CallOnScreenShake(1f);
            //print("Testing");
        }
    }
}
