﻿using UnityEngine;
using System.Collections;

public class PlayerUpRotation : MonoBehaviour {



    // Player up rotation stuff
    Vector3 curRot, lastRot;
    Vector3 lastFrameDif, frameDif;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        UpRotaion();

    }

    void UpRotaion()
    {
        curRot = transform.parent.position + transform.position;
        frameDif = lastRot - curRot;
        Vector3 smoothDif = ((frameDif - lastFrameDif) * 0.1f) + lastFrameDif;

        transform.up = smoothDif + Vector3.up * 2f;

        lastRot = curRot;
        lastFrameDif = frameDif;
    }
}
