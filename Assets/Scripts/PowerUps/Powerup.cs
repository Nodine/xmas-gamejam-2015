﻿using UnityEngine;
using System.Collections;

//is visible
// when colides, becomes invisible for x sec ( say 30 for now).

public class Powerup : MonoBehaviour {
    //private Collider col;
    private Renderer ren;
    private SphereCollider spCol;
    private float originalY;
    public float disappearTime = 5.0f;

    private AudioSource audioSource;

    // Use this for initialization

    void Start () {
        this.originalY = this.transform.position.y; // used to make the powerups bop up and down.
        //col = GetComponent<Collider>(); currently unused, pretains to the player hitting the powerup orbs.
        ren = GetComponent<Renderer>();
        spCol = GetComponent<SphereCollider>();
        audioSource = gameObject.GetComponent<AudioSource>();
        // getting playernr.
        
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name[col.transform.name.Length - 2].ToString() == "0")
        {
            EventManager.Instance.CallOnPowerUpPickUp(col.GetComponent<PlayerInput>().getPlayerNr); // add playernumber
            StartCoroutine(rePowerup(disappearTime)); // <-- removes the box, and the collider for float secounds
        }



    }

    IEnumerator rePowerup(float waittime)
    {

        spCol.enabled = false;
        ren.enabled = false;
        transform.GetChild(0).GetComponent<ParticleSystem>().Play();
        if (!audioSource.isPlaying)
            audioSource.Play();

        yield return new WaitForSeconds(waittime); // waits
        ren.enabled = true;
        spCol.enabled = true;
    }
    //aner ikke om vi skal bruge on trigger exit, men nu er den programmeret.
    void OnTriggerExit(Collider col)
    {
        //print("Superduper");
    }
	// Update is called once per frame
	void Update () {
        //måske kan vi få dem til at boppe op og ned?
        transform.position = new Vector3(transform.position.x, 0.3f * Mathf.Sin(Time.time) + originalY, transform.position.z);
        //måske en tjekket rotation ting ?
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }
}

