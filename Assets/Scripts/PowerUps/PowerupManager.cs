﻿using UnityEngine;
using System.Collections;

public class PowerupManager : MonoBehaviour {
    public static PowerupManager PuM;
    public int powerUpAmount = 3; //needs to be int (otherwise use floor).
    private static int[] powerup = new int[4];

    /* - 0 = inc_icon.png
    *  - 1 = burst_icon.png
    *  - 2 = water_icon.png
    */
    public static int[] Powerup { 
        get { return powerup; }
    }
    void OnEnable()
    {
        EventManager.onPickUp += grantPowerUp;
        EventManager.onUse += removePowerUp;
    }
    void OnDisable()
    {
        EventManager.onPickUp -= grantPowerUp;
        EventManager.onUse -= removePowerUp;
    }

    void grantPowerUp(int playerindex)
    {
        int powerupindex = Random.Range(0, 3);
        powerup[playerindex] = powerupindex;
    }

    void removePowerUp(int playerindex)
    {
        int powerupindex = -1;
        powerup[playerindex] = powerupindex;

    }
     
    // inverse controlls.
    // mess with gravity and friction.
    // Use this for initialization
    void Start () {
        for (int i = 0; i < 4; i++)
            powerup[i] = -1;
	}
	
	// Update is called once per frame
	void Update () {
	    
	}
}
