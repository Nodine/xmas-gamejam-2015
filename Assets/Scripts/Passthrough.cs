﻿using UnityEngine;
using System.Collections;

public class Passthrough : MonoBehaviour {

    RacePositionManager racePositionScript;
    int playerNr;
    int checkpointNr;
	// Use this for initialization
	void Start () {
        
        checkpointNr = int.Parse(this.transform.name[this.transform.name.Length - 1].ToString());
        racePositionScript = GameObject.Find("Managers/RaceManager").GetComponent<RacePositionManager>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider col)   
    {
        playerNr = int.Parse(col.gameObject.transform.name[col.gameObject.transform.name.Length - 1].ToString()) - 1;
        //Debug.Log("Detected player: " + playerNr + ", from Checkpoint: " + checkpointNr);
        racePositionScript.UpdateLastCheckpoint(playerNr, checkpointNr - 1);
        if (gameObject.name == "Checkpoint1")
            racePositionScript.CurPlayerPositions[racePositionScript.findPosInArray(playerNr, racePositionScript.CurPlayerPositions)].CurLaps++; 
    }
}
