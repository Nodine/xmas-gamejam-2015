﻿using UnityEngine;
using System.Collections;


public enum GameStates
{
    OnMenu,
    OnRunning,
    OnPause,
    OnEnd
};

public class GameManager : MonoBehaviour {

    public static GameManager Instance;
    RacePositionManager raceManagerScript;
    private GameStates gameState;
    int activePlayers = 4;

	GameObject[] players = new GameObject[4];

    [SerializeField]
    private GameObject SpawnPoint;

    [SerializeField]
    GameObject player1obj;
    [SerializeField]
    GameObject player2obj;
    [SerializeField]
    GameObject player3obj;
    [SerializeField]
    GameObject player4obj;
    private Texture win_bg;
    private Texture win_1;
    private Texture win_2;
    private Texture win_3;
    private Texture win_4;
    private Texture pwrup_bg;
    private Texture inc_icon;
    private Texture burst_icon;
    private Texture water_icon;

    AudioSource audio;
	
    void Awake(){
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);

        SpawnPoint = GameObject.FindGameObjectWithTag("Respawn");

        

        SpawnPlayer(0);//for testing purposes
        SpawnPlayer(1);
        SpawnPlayer(2);
        SpawnPlayer(3);
        
    }

    void OnEnable()
    {
        
    }

    void Start(){
        raceManagerScript = GameObject.Find("Managers/RaceManager").GetComponent<RacePositionManager>();
        EventManager.Instance.CallOnGameStart();
        Camera.main.SendMessage("SetNewTarget", players[0].transform, SendMessageOptions.DontRequireReceiver);
        win_bg = (Texture) Resources.Load("Sprites/win_screen_bg");
        win_1 = (Texture) Resources.Load("Sprites/win_1");
        win_2 = (Texture) Resources.Load("Sprites/win_2");
        win_3 = (Texture) Resources.Load("Sprites/win_3");
        win_4 = (Texture) Resources.Load("Sprites/win_4");
        pwrup_bg = (Texture)Resources.Load("Sprites/powerUpBox_bg");
        inc_icon = (Texture)Resources.Load("Sprites/inc_icon");
        burst_icon = (Texture)Resources.Load("Sprites/burst_icon");
        water_icon = (Texture)Resources.Load("Sprites/water_icon");
        audio = GetComponent<AudioSource>();
	}
    void Update(){

        //print(RacePositionManager.Instance.CurPlayerPositions[0].PlayerNr);
        for(int i = 0; i < 4; i++)
        {
            if(i != RacePositionManager.Instance.CurPlayerPositions[0].PlayerNr)
                if (CalcDistToLeader(RacePositionManager.Instance.CurPlayerPositions, i) > 9.0f && RacePositionManager.Instance.CurPlayerPositions[RacePositionManager.Instance.findPosInArray(i, RacePositionManager.Instance.CurPlayerPositions)].IsEnabled)
                {
                    EventManager.Instance.CallOnScreenShake(1f);
                    DisablePlayer(i);
                }
                    
        }

        if (activePlayers <= 1)
        {
            GameState = GameStates.OnEnd;
            audio.Play();
        }
        else if(activePlayers > 1)
        {
            GameState = GameStates.OnRunning;
        }

        //if (Input.anyKeyDown)
        //    someOnePressed = true; 
    }

	public void PauseGame(){
		//IMPLEMENT LATER!
	}

	// Properties
	public GameStates GameState { get; set;}


  //  bool someOnePressed = false;
    void OnGUI()
    {

        if (GameState == GameStates.OnEnd)
        {
            
            
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), win_bg, ScaleMode.ScaleToFit, true, 1.667f);
            if(RacePositionManager.Instance.CurPlayerPositions[RacePositionManager.Instance.findPosInArray(0, RacePositionManager.Instance.CurPlayerPositions)].IsEnabled)
                GUI.DrawTexture(new Rect(Screen.width * 0.5f, Screen.height * 0.5f, Screen.width / 4, Screen.height / 3), win_1);
            else if(RacePositionManager.Instance.CurPlayerPositions[RacePositionManager.Instance.findPosInArray(1, RacePositionManager.Instance.CurPlayerPositions)].IsEnabled)
            {
                GUI.DrawTexture(new Rect(Screen.width * 0.5f, Screen.height * 0.5f, Screen.width / 4, Screen.height / 3), win_2);
            }
            else if (RacePositionManager.Instance.CurPlayerPositions[RacePositionManager.Instance.findPosInArray(2, RacePositionManager.Instance.CurPlayerPositions)].IsEnabled)
            {
                GUI.DrawTexture(new Rect(Screen.width * 0.5f, Screen.height * 0.5f, Screen.width / 4, Screen.height / 3), win_3);
            }
            else if (RacePositionManager.Instance.CurPlayerPositions[RacePositionManager.Instance.findPosInArray(3, RacePositionManager.Instance.CurPlayerPositions)].IsEnabled)
            {
                GUI.DrawTexture(new Rect(Screen.width * 0.5f, Screen.height * 0.5f, Screen.width / 4, Screen.height / 3), win_4);
            }

            StartCoroutine(RestartGame());
        }

        if(GameState == GameStates.OnRunning)
        {
            {
                int powerUpSelect = PowerupManager.Powerup[0];
                switch(powerUpSelect)
                {
                    case 0:
                        GUI.Box(new Rect(20, 20, 50, 50), inc_icon);
                        break;
                    case 1:
                        GUI.Box(new Rect(20, 20, 50, 50), burst_icon);
                        break;
                    case 2:
                        GUI.Box(new Rect(20, 20, 50, 50), water_icon);
                        break;
                    case 3:
                        GUI.Box(new Rect(20, 20, 50, 50), pwrup_bg);
                        break;
                    default:
                        GUI.Box(new Rect(20, 20, 50, 50), pwrup_bg);
                        break;
                }
            }

            {
                int powerUpSelect = PowerupManager.Powerup[1];
                switch (powerUpSelect)
                {
                    case 0:
                        GUI.Box(new Rect(Screen.width - 70, 20, 50, 50), inc_icon);
                        break;
                    case 1:
                        GUI.Box(new Rect(Screen.width - 70, 20, 50, 50), burst_icon);
                        break;
                    case 2:
                        GUI.Box(new Rect(Screen.width - 70, 20, 50, 50), water_icon);
                        break;
                    case 3:
                        GUI.Box(new Rect(Screen.width - 70, 20, 50, 50), pwrup_bg);
                        break;
                    default:
                        GUI.Box(new Rect(Screen.width - 70, 20, 50, 50), pwrup_bg);
                        break;
                }
            }

            {
                int powerUpSelect = PowerupManager.Powerup[2];
                switch (powerUpSelect)
                {
                    case 0:
                        GUI.Box(new Rect(20, Screen.height - 70, 50, 50), inc_icon);
                        break;
                    case 1:
                        GUI.Box(new Rect(20, Screen.height - 70, 50, 50), burst_icon);
                        break;
                    case 2:
                        GUI.Box(new Rect(20, Screen.height - 70, 50, 50), water_icon);
                        break;
                    case 3:
                        GUI.Box(new Rect(20, Screen.height - 70, 50, 50), pwrup_bg);
                        break;
                    default:
                        GUI.Box(new Rect(20, Screen.height - 70, 50, 50), pwrup_bg);
                        break;
                }
            }
            {
                int powerUpSelect = PowerupManager.Powerup[3];
                switch (powerUpSelect)
                {
                    case 0:
                        GUI.Box(new Rect(Screen.width - 70, Screen.height - 70, 50, 50), inc_icon);
                        break;
                    case 1:
                        GUI.Box(new Rect(Screen.width - 70, Screen.height - 70, 50, 50), burst_icon);
                        break;
                    case 2:
                        GUI.Box(new Rect(Screen.width - 70, Screen.height - 70, 50, 50), water_icon);
                        break;
                    case 3:
                        GUI.Box(new Rect(Screen.width - 70, Screen.height - 70, 50, 50), pwrup_bg);
                        break;
                    default:
                        GUI.Box(new Rect(Screen.width - 70, Screen.height - 70, 50, 50), pwrup_bg);
                        break;
                }
            }
            
            
            
        }
    }
    
        
    IEnumerator RestartGame()
    {
        //while(!someOnePressed)
        //{
        //    yield return null;
        //}

        yield return new WaitForSeconds(3f);
        Application.LoadLevel(Application.loadedLevel);
    }

    public float CalcDistToLeader(RacePositionManager.RacePosition[] curStandings, int playerNr)
    {
        GameObject curLeader = GameObject.Find("player0" + (curStandings[0].PlayerNr + 1)); // trying something here: original below.
        //GameObject curLeader = GameObject.Find("player0" + (curStandings[0].PlayerNr + 1));
        GameObject inqObj = GameObject.Find("player0" + (playerNr + 1));

        Debug.DrawLine(curLeader.transform.position, inqObj.transform.position, Color.green);
        //Debug.Log(Vector3.Distance(inqObj.transform.position, curLeader.transform.position));
        Debug.Log(Mathf.Abs((curLeader.transform.position - inqObj.transform.position).magnitude));
        return Mathf.Abs((curLeader.transform.position - inqObj.transform.position).magnitude);
    }

	public void SpawnPlayer(int playerNr){
        //Debug.Log("Player nr: " + playerNr);
        switch (playerNr)
        {
            case 0:
                players[playerNr] = (GameObject) Instantiate(player1obj);
                break;
            case 1:
                players[playerNr] = (GameObject) Instantiate(player2obj);
                break;
            case 2:
                players[playerNr] = (GameObject) Instantiate(player3obj);
                break;              
            case 3:                 
                players[playerNr] = (GameObject) Instantiate(player4obj);
                break;              
            default:                
                players[playerNr] = (GameObject) Instantiate(player1obj);
                break;
        }
            
		players[playerNr].transform.position = new Vector3(/*1.1f * (float)playerNr +*/ SpawnPoint.transform.position.x, SpawnPoint.transform.position.y, 1.1f * (float)playerNr + SpawnPoint.transform.position.z);
		players[playerNr].name = "player0" + (playerNr + 1);
	}

	public void DeSpawnPlayer(int playerNr){
		Destroy (players[playerNr]);
	}

	public void DisablePlayer(int playerNr){
        players[playerNr].GetComponentInChildren<MeshRenderer>().enabled = false;
        players[playerNr].GetComponentInChildren<ParticleSystem>().emissionRate = 0.0f;
        players[playerNr].GetComponent<BoxCollider>().enabled = false;
        players[playerNr].GetComponent<MovementManager>().enabled = false;
        players[playerNr].GetComponent<PlayerInput>().enabled = false;
        RacePositionManager.Instance.CurPlayerPositions[RacePositionManager.Instance.findPosInArray(playerNr, RacePositionManager.Instance.CurPlayerPositions)].IsEnabled = false;
        activePlayers--;
	}

	public void EnablePlayer(int playerNr){
		if (IsPlayerSpawned(playerNr)) {
			if (IsPlayerActive(playerNr)){
				players[playerNr].SetActive (true);
			}
		}
		else {
			SpawnPlayer(playerNr);
		}
	}

	public bool IsPlayerActive(int playerNr){
		return players[playerNr].activeSelf;
	}

	public bool IsPlayerSpawned(int playerNr){
		return players[playerNr] == null ? false : true;
	}

	public GameObject GetPlayerObject(int playerNr){
		return players[playerNr];
	}

    public GameObject GetLeader()
    {
        // RacePositionManager.RacePosition[] curStandings
        GameObject curLeader = GameObject.Find("player0" + (RacePositionManager.Instance.CurPlayerPositions[0].PlayerNr + 1));
        //print(curLeader.name);
        return curLeader;
    }
}
