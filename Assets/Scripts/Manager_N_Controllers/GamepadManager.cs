 using System.Linq;
using UnityEngine;
using XInputDotNetPure;

public class GamepadManager : MonoBehaviour {

	GamePadState[] state = new GamePadState[4];
	GamePadState[] prevState = new GamePadState[4];

	GameManager gameManagerScript;

    void Start()
    {
		gameManagerScript = gameObject.GetComponent<GameManager>();
		int j = 0;
        for (int i = 0; i < 4; i++)
        {
            state[i] = GamePad.GetState((PlayerIndex)i);
            if (state[i].IsConnected){ 
				Debug.Log(string.Format("Gamepad {0} have connected to the game!", (PlayerIndex)i));
				j++;
			}
        }
		Debug.Log("There are " + j + " controllers connected!");
    }

    void Update ()
	{
		for (int i = 0; i < 4; i++)
		{
	    	prevState[i] = state[i];
	    	state[i] = GamePad.GetState((PlayerIndex)i);
	    
			if (state[i].IsConnected && !prevState[i].IsConnected)
			{
				Debug.Log(string.Format("Gamepad {0} have reconnected to the game!", (PlayerIndex)i));
				gameManagerScript.EnablePlayer(i);
			}
			else if(!state[i].IsConnected && prevState[i].IsConnected)
			{
				Debug.Log(string.Format("Gamepad {0} have disconnected from the game!", (PlayerIndex)i));
				gameManagerScript.PauseGame();
				gameManagerScript.DisablePlayer(i);
			}
		}
	}
}