﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RacePositionManager : MonoBehaviour {

    public static RacePositionManager Instance;

    RacePosition[] curPlayerPositions;
    [SerializeField]
    GameObject[] checkPointList;

    public RacePosition[] CurPlayerPositions{
        get { return curPlayerPositions; }
}


    void Awake()
    {
        if (Instance == null)
            Instance = this;
        //else
        //    Destroy(this);
    }

    public void UpdateLastCheckpoint(int playerNr, int checkPoint)
    {
        curPlayerPositions[findPosInArray(playerNr, curPlayerPositions)].CurrentCheckpoint = checkPoint;
    }

    public struct RacePosition
    {
        int playerNr;
        int currentCheckpoint;
        float distanceToNextCheckpoint;
        int curLaps;
        bool isEnabled;
        public int CurrentCheckpoint
        {
            get { return currentCheckpoint; }
            set { currentCheckpoint = value; }
        }

        public float DistanceToNextCheckpoint
        {
            get { return distanceToNextCheckpoint; }
            set { distanceToNextCheckpoint = value; }
        }
        public int PlayerNr
        {
            get { return playerNr; }
            set { playerNr = value; }
        }
        public int CurLaps
        {
            get { return curLaps; }
            set { curLaps = value; }
        }
        public bool IsEnabled
        {
            get { return isEnabled; }
            set { isEnabled = value; }
        }

    }

    public int findPosInArray(int playerNr, RacePosition[] rpArray)
    {
        int posInArray = 0;
        for (int i = 0; i < 4; i++)
        {
            if (rpArray[i].PlayerNr == playerNr)
                posInArray = i;
        }
        return posInArray;
    }
    
    float CalcDistanceToNextCheckPoint(int playerNr)
    {
        int nextCheckPoint;
        GameObject playerObj;
        //Debug.Log("Finding player0" + (playerNr + 1));
        playerObj = GameObject.Find("player0" + (playerNr + 1));
        int posInPositions = findPosInArray(playerNr, curPlayerPositions);
        if (curPlayerPositions[posInPositions].CurrentCheckpoint == 6)
            nextCheckPoint = 0;
        else
            nextCheckPoint = curPlayerPositions[posInPositions].CurrentCheckpoint + 1;
        //Debug.Log("Current Checkpoint: " + curPlayerPositions[posInPositions].CurrentCheckpoint);
        //Debug.Log("Next checkpoint: " + nextCheckPoint);
        Vector3 checkpointPos = checkPointList[nextCheckPoint].transform.position;
        
        Vector3 playerPos;
        playerPos = playerObj.transform.position;
        //Debug.DrawLine(playerPos, checkpointPos, Color.green);
        return (playerPos - checkpointPos).magnitude;
    }

    RacePosition[] racePositionSort(RacePosition[] rpArray)
    {
        List<RacePosition> oldRacePosition = new List<RacePosition>();
        for (int i = 0; i < rpArray.Length; i++)
        {
            oldRacePosition.Add(rpArray[i]);
        }
        List<RacePosition> newRacePosition = new List<RacePosition>();

        newRacePosition.Add(cmpRacePositions(cmpRacePositions(oldRacePosition[0], oldRacePosition[1]),cmpRacePositions(oldRacePosition[2], oldRacePosition[3])));
        oldRacePosition.Remove(cmpRacePositions(cmpRacePositions(oldRacePosition[0], oldRacePosition[1]), cmpRacePositions(oldRacePosition[2], oldRacePosition[3])));

        newRacePosition.Add(cmpRacePositions(cmpRacePositions(oldRacePosition[0], oldRacePosition[1]),oldRacePosition[2]));
        oldRacePosition.Remove(cmpRacePositions(cmpRacePositions(oldRacePosition[0], oldRacePosition[1]), oldRacePosition[2]));

        newRacePosition.Add(cmpRacePositions(oldRacePosition[0], oldRacePosition[1]));
        oldRacePosition.Remove(cmpRacePositions(oldRacePosition[0], oldRacePosition[1]));
        newRacePosition.Add(oldRacePosition[0]);

        RacePosition[] newRacePositionArray = new RacePosition[4];
        newRacePositionArray = newRacePosition.ToArray();
        return newRacePositionArray;
    }

    RacePosition cmpRacePositions(RacePosition position1, RacePosition position2)
    {
        if (position1.CurLaps > position2.CurLaps)
            return position1;
        else if (position2.CurLaps > position1.CurLaps)
            return position2;
        else if (position1.CurLaps == position2.CurLaps)
        {
            if (position1.CurrentCheckpoint > position2.CurrentCheckpoint)
                return position1;
            else if (position2.CurrentCheckpoint > position1.CurrentCheckpoint)
                return position2;
            else if (position1.CurrentCheckpoint == position2.CurrentCheckpoint)
            {
                if (position1.DistanceToNextCheckpoint < position2.DistanceToNextCheckpoint)
                    return position1;
                else if (position2.DistanceToNextCheckpoint < position1.DistanceToNextCheckpoint)
                    return position2;
                else if (position1.DistanceToNextCheckpoint == position2.DistanceToNextCheckpoint)
                    return position1;
                else
                    return position1;
            }
            else
                return position1;
        }
        else
            return position1;
        

    }

    void printRacePositions(RacePosition[] rpArray){
        for(int i = 0; i < rpArray.Length; i ++)
        {
            Debug.Log("In pos " + (i + 1) + " : " + (rpArray[i].PlayerNr + 1) + "At checkpoint: " + rpArray[i].CurrentCheckpoint + " on lap: " + rpArray[i].CurLaps);
        }
}

	// Use this for initialization
	void Start () {
        
        //Find checkpoints
        checkPointList = new GameObject[7];
        for (int i = 0; i < checkPointList.Length; i++)
        {
            //Debug.Log(i);
            checkPointList[i] = GameObject.Find("Checkpoint" + (i + 1));
        }

        curPlayerPositions = new RacePosition[4];
        for (int i = 0; i < curPlayerPositions.Length; i++)
        {
            curPlayerPositions[i].CurrentCheckpoint = 0;
            curPlayerPositions[i].PlayerNr = i;
            curPlayerPositions[i].DistanceToNextCheckpoint = CalcDistanceToNextCheckPoint(curPlayerPositions[i].PlayerNr);
            curPlayerPositions[i].IsEnabled = true;
        }

         
	}

    
	
	// Update is called once per frame
	void Update () {
	    //Update the current standings, happens every frame.
        for (int i = 0; i < 4; i ++)
        {
            //Debug.Log(i);
            curPlayerPositions[i].DistanceToNextCheckpoint = CalcDistanceToNextCheckPoint(curPlayerPositions[i].PlayerNr);
            //Debug.Log("Distance: " + curPlayerPositions[i].DistanceToNextCheckpoint);
        }
        curPlayerPositions = racePositionSort(curPlayerPositions);
        printRacePositions(curPlayerPositions);

	}
}
