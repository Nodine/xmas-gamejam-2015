﻿using UnityEngine;
using System.Collections;

public class EventManager : MonoBehaviour {

    //The intention of this script is to create events.
    //The events can be activated from other scripts and dealt with there. 
    // ______ 
    // I you want to activate an event from a different script just write:
    // EventManager.Instance.CallOnXXXXXXX();
    // Where XXXXXX is replaced with the event in question.
    // This will in turn make other scripts, listening for that event,
    // activate their subscribed function.
    
         
    public static EventManager Instance;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);
    }


    // When the game starts
    public delegate void GameStart();
    public static GameStart onGameStart;

    // When a level is being selected
    public delegate void LevelSelect();
    public static LevelSelect onLevelSelect;

    //When the match starts
    public delegate void MatchStart();
    public static MatchStart onMatchStart;

    // When pausing/unpausing the game
    public delegate void PauseGame(bool isPaused);
    public static event PauseGame onPause;

    // When the winner is found
    public delegate void WinnerFound(int player);
    public static WinnerFound onWin;

    // When a player triggers a power up
    public delegate void PickUpPower(int playerNr);
    public static PickUpPower onPickUp;

    public delegate void UsePower(int playerNr);
    public static UsePower onUse;


    // When a screen shake should happen
    public delegate void ScreenShaker(float shakeTime);
    public static event ScreenShaker onScreenShake;

    /* *************
    Event Calls
    ************** */

    public void CallOnLevelSelect()
    {
        if (onLevelSelect != null)
            onLevelSelect();
    }

    public void CallOnGameStart()
    {
        if (onGameStart != null)
            onGameStart();
    }

    public void CallOnMatchStart()
    {
        if (onMatchStart != null)
            onMatchStart();
    }

    public void CallOnPause(bool aVal)
    {
        if (onPause != null)
            onPause(aVal);
    }

    public void CallOnWin(int playerNr)
    {
        if (onWin != null)
            onWin(playerNr);
    }

    public void CallOnPowerUpPickUp(int playerNr)
    {
        if (onPickUp != null)
            onPickUp(playerNr);
    }

    public void CallOnPowerUse(int playerNr)
    {
        if (onUse != null)
            onUse(playerNr);
    }

    public void CallOnScreenShake(float shakeTime)
    {
        if (onScreenShake != null)
            onScreenShake(shakeTime);
    }


}
