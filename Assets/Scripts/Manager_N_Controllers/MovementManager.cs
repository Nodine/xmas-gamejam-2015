﻿using UnityEngine;
using System.Collections;

public class MovementManager : MonoBehaviour {

    [SerializeField]
    float leftHorizontalInput;
    [SerializeField]
    float leftVerticalInput;
    [SerializeField][Range(0.0f, 1000.0f)]
    float forceMult;
    [SerializeField]
    float globalForceMult;
    
    GameObject UpDirector;
    GameObject LookDirector;

    public float GlobalForceMult
    {
        get { return globalForceMult; }
        set { globalForceMult = value; }
    }

    const float powertime = 3.0f;

    float soakedTimeStamp = 0f;

    float timeStamp;

    public float TimeStamp
    {
        get { return timeStamp; }
        set { timeStamp = value; }
    }

    float gradTimeStamp;

    public float GradTimeStamp
    {
        get { return gradTimeStamp; }
        set { gradTimeStamp = value; }
    }

    private bool isPoweredUp = false;

    public bool IsPoweredUp
    {
        get { return isPoweredUp; }
        set { isPoweredUp = value; }
    }

    Rigidbody playerRigidBody;
    PlayerInput playerInputScript;
    public WaterbaloonScript waterballoon;

    Vector2 playerPos; 
    float deltaY;
    float deltaX;
    float angleInDegrees;

    private bool soaked;
    public bool Soaked
    {
        get { return soaked; }
        set { soaked = value; }
    }

    private bool isAimingInFront;
    public bool IsAimingInFront
    {
        get { return isAimingInFront; }
    }

    [SerializeField][Range(0.0f, 1.0f)]
    float triggerDeadzone;

    void shootPowerUp(bool aimDirection)
    {
        int powerUpSelect = PowerupManager.Powerup[playerInputScript.getPlayerNr];

        // speedboosts can be independent of aimdirection
        switch (powerUpSelect)
        {
            case 0:
                IsPoweredUp = true; //inc_icon.png
                TimeStamp = Time.time + powertime;
                speedPowerUpGrad(powertime);
                break;
            case 1: //burst_icon.png
                IsPoweredUp = true;
                speedPowerUpConst(powertime);
                break;
            case 2: //water_icon.png
                IsPoweredUp = true;
                waterBalloon();
                break;

        }
        EventManager.Instance.CallOnPowerUse(playerInputScript.getPlayerNr);
    }

	// Use this for initialization
	void Start () {
        playerRigidBody = gameObject.GetComponent<Rigidbody>();
        forceMult = 250.0f;
        globalForceMult = 1.0f;
        playerPos = new Vector2(gameObject.transform.position.x, gameObject.transform.position.z);
        playerInputScript = gameObject.GetComponent<PlayerInput>();
        bool isAimingInFront = true;

        LookDirector = transform.GetChild(0).gameObject;
        UpDirector = LookDirector.transform.GetChild(0).gameObject;
	}
	
	// Update is called once per frame
	void Update () {
        /*
        deltaY = playerInputScript.getRThumbStickVector.y - playerPos.y;
        deltaX = playerInputScript.getRThumbStickVector.x - playerPos.x;
        angleInDegrees = Mathf.Atan2(deltaY, deltaX) * 180 / Mathf.PI;
        */
        leftHorizontalInput = playerInputScript.getLThumbStickVector.x;
        leftVerticalInput = playerInputScript.getLThumbStickVector.y;

        if (playerInputScript.getRThumbStickVector.x < 0)
            isAimingInFront = false;
        else
            isAimingInFront = true;

        //can you use a powerup currently?
        if (playerInputScript.getRightTrigger > 0 + triggerDeadzone && IsPoweredUp == false)
        {
            shootPowerUp(isAimingInFront);
        }

        if (Mathf.Abs(playerInputScript.getLThumbStickVector.magnitude) > 0)
        {
            
            Vector3 tmp = transform.position + new Vector3(playerInputScript.getLThumbStickVector.x, 0, playerInputScript.getLThumbStickVector.y);
            transform.transform.LookAt(tmp);
           // LookDirector.transform.rotation = Quaternion.LookRotation(tmp);
        //    UpRotation();
            //transform.forward = tmp;

        }
        if (timeStamp < Time.time && IsPoweredUp == true)
        {
            removeSpeedUp();
            removePoweredUp();
        }


        if(GradTimeStamp < Time.time && IsPoweredUp == true)
        {
            Debug.Log("yup");
            speedPowerUpGrad(powertime);
        }

        //checks if you are soaked
        if(Soaked == true)
        { 
            this.playerRigidBody.mass = this.playerRigidBody.mass * 2;
            Soaked = false;
            soakedTimeStamp = Time.time + 2f;

        }

        if (this.playerRigidBody.mass > 15 && soakedTimeStamp < Time.time)
        {
            soakedTimeStamp = 0f;
            this.playerRigidBody.mass = this.playerRigidBody.mass / 2;
        }
    }

    void removePoweredUp()
    {
        IsPoweredUp = false;
    }
    void removeSpeedUp()
    {
        GlobalForceMult = 1f;
    }

    void FixedUpdate() {
        playerRigidBody.AddForce(new Vector3(leftHorizontalInput * forceMult * globalForceMult, 0, leftVerticalInput * forceMult * globalForceMult));
    }

    //gradual speed powerup.
    void speedPowerUpGrad(float powertime)
    {
        GradTimeStamp = Time.time + 0.5f;
        GlobalForceMult = GlobalForceMult * 1.05f;
    }

    //constant speed powerup.
    void speedPowerUpConst(float powertime)
    {
        TimeStamp = Time.time + powertime;
        gradTimeStamp = Time.time + 10;
        GlobalForceMult = GlobalForceMult * 1.2f;
    }


    // Player up rotation stuff
    Vector3 curRot, lastRot;
    Vector3 lastFrameDif, frameDif;

    void UpRotation()
    {
       // Vector3 tmp = transform.position + new Vector3(playerInputScript.getLThumbStickVector.x, 0, playerInputScript.getLThumbStickVector.y);

        curRot =  UpDirector.transform.position;
        frameDif = lastRot - curRot;
        Vector3 smoothDif = ((frameDif - lastFrameDif) * 0.1f) + lastFrameDif;
        Debug.DrawLine(UpDirector.transform.position, smoothDif * 10f, Color.blue);
       // UpDirector.transform.up = smoothDif + Vector3.up * 2f;
        UpDirector.transform.LookAt(UpDirector.transform.position + smoothDif + Vector3.up * 2f);
        lastRot = curRot;
        lastFrameDif = frameDif;
    }

    void waterBalloon()
    {
        //generates and shoots a waterballoon;
        Vector3 playerDirection = transform.forward;
        //Quaternion playerRotation = this.transform.rotation + this.transform.rotation.z +90;
        //Quaternion playerRotation2 = (this.transform.rotation.x, this.transform.rotation.y, this.transform.rotation.z+90);
        float spawnDistance = 1;
        if (!isAimingInFront)
        {
            playerDirection = -playerDirection;
        }
        Vector3 projectilePosition = this.transform.position + playerDirection * spawnDistance;

        GameObject clone = (GameObject)Instantiate(Resources.Load("Prefabs/WaterBalloon"), projectilePosition, transform.rotation);
    }
}
