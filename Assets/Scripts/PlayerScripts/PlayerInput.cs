﻿using UnityEngine;
using XInputDotNetPure;
using System.Collections;

public class PlayerInput : MonoBehaviour {
	
    Vector2 rThumbStickVector = new Vector2 (0.0f, 0.0f);
    float rTrigger;
    Vector2 lThumbStickVector = new Vector2 (0.0f, 0.0f);
    float lTrigger;

    int playerNr;
    GamePadState state;
    GamePadState prevState;

    public int getPlayerNr
    {
        get { return playerNr; }
    }

    public Vector2 getRThumbStickVector {
        get { return rThumbStickVector; }
    }

    public Vector2 getLThumbStickVector {
        get { return lThumbStickVector; }
    }

    public float getRightTrigger
    {
        get { return rTrigger; }
    }

    public float getLeftTrigger
    {
        get { return lTrigger; }
    }


    void Start()
    {
        playerNr = int.Parse(this.transform.name[this.transform.name.Length - 1].ToString()) - 1;
    }

    void Update() {
        prevState = state;
        state = GamePad.GetState((PlayerIndex)playerNr);
        if (state.IsConnected)
        {
	        if (prevState.Buttons.A == ButtonState.Released && state.Buttons.A == ButtonState.Pressed)
	            Debug.Log(string.Format("Player {0} pressed A!", playerNr + 1));

	        if (prevState.Buttons.B == ButtonState.Released && state.Buttons.B == ButtonState.Pressed)
	            Debug.Log(string.Format("Player {0} pressed B!", playerNr + 1));

	        if (prevState.Buttons.X == ButtonState.Pressed && state.Buttons.X == ButtonState.Pressed)
	            Debug.Log(string.Format("Player {0} pressed X!", playerNr + 1));

	        if (prevState.Buttons.Y == ButtonState.Released && state.Buttons.Y == ButtonState.Pressed)
	            Debug.Log(string.Format("Player {0} pressed Y!", playerNr + 1));

	        if (prevState.Buttons.LeftShoulder == ButtonState.Released && state.Buttons.LeftShoulder == ButtonState.Pressed)
	            Debug.Log(string.Format("Player {0} pressed LB!", playerNr + 1));

	        if (prevState.Buttons.RightShoulder == ButtonState.Released && state.Buttons.RightShoulder == ButtonState.Pressed)
	            Debug.Log(string.Format("Player {0} pressed RB!", playerNr + 1));
	        if (prevState.Buttons.Back == ButtonState.Released && state.Buttons.Back == ButtonState.Pressed)
	            Debug.Log(string.Format("Player {0} pressed Back!", playerNr + 1));

	        rThumbStickVector = new Vector2(state.ThumbSticks.Right.X, state.ThumbSticks.Right.Y);
            rTrigger = state.Triggers.Right;
	        lThumbStickVector = new Vector2(state.ThumbSticks.Left.X, state.ThumbSticks.Left.Y);
            lTrigger = state.Triggers.Left;
    	}
	}
}
