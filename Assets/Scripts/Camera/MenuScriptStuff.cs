﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class MenuScriptStuff : MonoBehaviour {


    [SerializeField]
    CanvasGroup PressButton;

    [SerializeField]
    CanvasGroup MainMenu;

    [SerializeField]
    CanvasGroup JoystickCanvas;

    [SerializeField]
    Image img1;
    [SerializeField]
    Image img2;
    [SerializeField]
    Image img3;

    bool buttonDown = false;

	// Use this for initialization
	void Start ()
    {
        img1.gameObject.SetActive(false);
        img2.gameObject.SetActive(false);
        img3.gameObject.SetActive(false);

        StartCoroutine(MenuStuff());
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.anyKeyDown)
        {
            buttonDown = true;
        }
	}

    IEnumerator MenuStuff()
    {
        while(!buttonDown)
        {
            float t = 0;

            while(t < 1)
            {
                t += Time.deltaTime;
                PressButton.alpha = t;
                yield return null;
            }

            while (t > 0)
            {
                t -= Time.deltaTime;
                PressButton.alpha = t;
                yield return null;
            }
        }

        float t1 = 0;
        while (t1 < 1)
        {
            t1 += Time.deltaTime;

            MainMenu.alpha = 1 - t1;
            JoystickCanvas.alpha = t1;
            yield return null;
        }


        yield return new WaitForSeconds(4f);
        JoystickCanvas.alpha = 0;
        img3.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        img3.gameObject.SetActive(false);
        img2.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        img2.gameObject.SetActive(false);
        img1.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);

        Application.LoadLevel(Application.loadedLevel + 1);
    }
}
