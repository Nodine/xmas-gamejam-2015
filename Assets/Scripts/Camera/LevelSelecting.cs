﻿using UnityEngine;
using System.Collections;

public class LevelSelecting : MonoBehaviour {

    [SerializeField]
    private GameObject[] levels;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        //RotateExample(0);
	}

    private void RotateExample(int i)
    {
        //levels[i].transform.localEulerAngles = 
        levels[i].transform.Rotate(this.transform.up, 15f * Time.deltaTime);
    }
}
