﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {


    [SerializeField]
    Transform target;

    [SerializeField]
    Vector3 offset = new Vector3(0, 10, 0);

    [SerializeField]
    float dampTime = 8f;

  //  [SerializeField]
  //  float screenShakeTime = 1f;
    bool isShaking = false;

    Vector3 velocity;

    void OnEnable()
    {
        EventManager.onScreenShake += OnScreenShake;
    }

    void OnDisable()
    {
        EventManager.onScreenShake -= OnScreenShake;
    }


	// Use this for initialization
	void Start ()
    {
        // IF no target go to origin. 
        if (target == null)
            target = GameManager.Instance.transform;
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 destination;

        target = GameManager.Instance.GetLeader().transform;

        destination = (target.position + offset);

        if (Input.GetKeyDown(KeyCode.S))
            EventManager.Instance.CallOnScreenShake(0.5f);

        if (!isShaking)
            transform.position = Vector3.Lerp(transform.position, destination, 0.2f);
    }


    public void SetNewTarget(Transform aTarget)
    {
        target = aTarget;
    }

    public void OnScreenShake(float shakeTime)
    {
        StartCoroutine(ScreenShaking(shakeTime));
    }

    IEnumerator ScreenShaking(float shakeTime)
    {
        float t = 0;
        isShaking = true;
        Vector3 shakeOffset = Vector3.zero;
        shakeOffset.y = offset.y;

        while(t < shakeTime)
        {
            t += Time.deltaTime;
            shakeOffset.x =  Mathf.PerlinNoise(Time.time * 20, shakeOffset.x);
            shakeOffset.z =     Mathf.PerlinNoise(Time.time * 20 * t, shakeOffset.z);
            transform.position = target.position + shakeOffset;//Vector3.Lerp(transform.position, destination, dampTime );
            yield return null;
        }

        isShaking = false;
      //  yield return null;
    }
}
