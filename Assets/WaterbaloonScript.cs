﻿using UnityEngine;
using System.Collections;

public class WaterbaloonScript : MonoBehaviour {

    

    void OnTriggerEnter(Collider col)
    {

        if (col.tag == "Untagged")
        {
            MovementManager colManager = col.GetComponent<MovementManager>();

            colManager.Soaked = true;
            // do stuff, then destroy!
            //Debug.Log("sup");
            
            Destroy(this.gameObject);
        }
        else
            Destroy(this.gameObject);
    }
	// Use this for initialization
	void Start () {
        this.transform.Rotate(0, 0, 90);
	}
    int speed = 10;
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }
}
